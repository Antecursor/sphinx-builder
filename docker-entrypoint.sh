#!/bin/sh
set -e

. ${WORKDIR}/venv/bin/activate

exec "${@}"
