#
# Sphinx
#

# Aguments (preprocessors)
ARG WORKDIR=/data
ARG PYTHON_IMAGE=3.7-alpine

FROM python:${PYTHON_IMAGE:-alpine} AS builder

# Local builder env.
# We're not using ENV for workdir since it's a
# shared path between builder and runner.
ARG WORKDIR
ENV WORKDIR ${WORKDIR}

## Artifacts directory.
RUN mkdir -p ${WORKDIR}/
WORKDIR ${WORKDIR}/

## Create virtualenv.
RUN pip install virtualenv \
    && virtualenv --always-copy --download ${WORKDIR}/venv \
    && virtualenv --relocatable ${WORKDIR}/venv

# Copy software dependencies list.
COPY requirements.txt ${WORKDIR}/requirements.txt

## Install modules.
RUN . ${WORKDIR}/venv/bin/activate \
    && pip install -r ${WORKDIR}/requirements.txt

FROM python:${PYTHON_IMAGE:-alpine} AS runner

# Local runner env.
ARG WORKDIR
ENV WORKDIR ${WORKDIR}

# Copy virtualenv from builder.
COPY --from=builder ${WORKDIR}/venv ${WORKDIR}/venv
WORKDIR ${WORKDIR}/

# Install gcc.
RUN apk add make

# Add entrypoint (virtualenv loading).
COPY docker-entrypoint.sh ${WORKDIR}/
RUN chmod +x ${WORKDIR}/docker-entrypoint.sh
ENTRYPOINT ["./docker-entrypoint.sh"]
